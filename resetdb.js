var pgp = require('pg-promise')();
var db = pgp('postgres://postgres:password@postgres:5432/');

db.task(async(t) => {
  await t.none('DROP DATABASE IF EXISTS true_sizer');
  await t.none('CREATE DATABASE true_sizer');
})
  .then(() => {
    db2 = pgp('postgres://postgres:password@postgres:5432/true_sizer')
    db2.task(async(t) => {
      await t.none('DROP TABLE IF EXISTS size_ratings');
      await t.none('DROP TABLE IF EXISTS shoes');
      await t.none('CREATE TABLE shoes(id SERIAL PRIMARY KEY, name VARCHAR(255))');
      await t.none('CREATE TABLE size_ratings(id SERIAL PRIMARY KEY, shoe_id INTEGER REFERENCES shoes(id), rating INTEGER)');
      await t.none("INSERT INTO shoes(name) VALUES ('adidas Yeezy'), ('Air Jordan'), ('Nike atmos')");
    })
      .then(() => pgp.end())
      .catch((e) => {
        console.log(e);
        pgp.end();
      });
  })
  .catch((e) => {
    console.log(e);
    pgp.end();
  });
