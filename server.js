'use strict';

const express = require('express');
const bodyParser = require('body-parser');

const PORT = 8080;
const HOST = '0.0.0.0';

const app = express();
const db = require('./queries');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.send("Welcome to True Sizer");
});

app.get('/shoes', db.getAllShoes);
app.get('/shoes/:id', db.getOneShoe);
app.post('/shoes/:id/rate', db.createSizeRating);

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
