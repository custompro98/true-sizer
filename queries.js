var pgp = require('pg-promise')();
var db = pgp('postgres://postgres:password@postgres:5432/true_sizer');

getAllShoes = (req, res, next) => {
  db.any('SELECT shoes.id, name, ARRAY_AGG(sr.rating), AVG(sr.rating) FROM shoes LEFT JOIN size_ratings sr ON sr.shoe_id = shoes.id GROUP BY shoes.id, name')
    .then(data => {
      res.status(200)
        .json({
          status: 'success',
          data: data
        });
    })
    .catch(e => next(e));
};

getOneShoe = (req, res, next) => {
  var id = parseInt(req.params.id);

  db.one('SELECT shoes.id, name, ARRAY_AGG(sr.rating), AVG(sr.rating) FROM shoes LEFT JOIN size_ratings sr ON sr.shoe_id = shoes.id WHERE shoes.id = $1 GROUP BY shoes.id, name', id)
    .then(data => {
      res.status(200)
        .json({
          status: 'success',
          data: data
        });
    })
    .catch(e => next(e));
};

createSizeRating = (req, res, next) => {
  var id = parseInt(req.params.id);
  var rating = parseInt(req.body.rating);

  if (rating < 1 || rating > 5) {
    res.status(400)
      .json({
        status: 'bad_request',
        message: 'Rating must be between 1 & 5'
      });
  } else {
    db.none('INSERT INTO size_ratings(shoe_id, rating) VALUES($1, $2)', [id, rating])
      .then(() => {
        res.status(200)
          .json({
            status: 'success'
          });
      })
      .catch(e => next(e));
  }
};

module.exports = {
  getAllShoes: getAllShoes,
  getOneShoe: getOneShoe,
  createSizeRating: createSizeRating
};
