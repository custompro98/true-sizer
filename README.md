# True Sizer
Calculate a shoe's true-to-size rating based on community data

## Setup
This project uses Docker and docker-compose.
- [Install Docker (Ubuntu)](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
- [Install Docker (Mac)](https://docs.docker.com/docker-for-mac/install/)
- [Install docker-compose](https://docs.docker.com/compose/install/)

Run the following steps from your shell:
1. Build the project: `docker-compose build web`
2. Start the database: `docker-compose up -d postgres`
3. Setup the database: `docker-compose run --rm web npm run resetdb`
4. Start the server: `docker-compose up web`

## Usage
Use Postman collection in the [repo](TrueSizer.postman_collection.json) to make requests to the running server.

### GET Shoes
Returns details about all of the shoes that exist in the database
Format:
```
{
  status,
  data: [
    {
      id,
      name,
      ratings,
      avg
    }
  ]
}
```

### GET Shoe
Returns details about the requested shoe
Format:
```
{
  status,
  data: {
    id,
    name,
    ratings,
    avg
  }
}
```

### POST Size Rating
Submits a rating (1-5) of the specified shoe
Format:
```
{
  status
}
```
